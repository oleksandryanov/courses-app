# Courses React App

![Overview](OVERVIEW.png)

## Overview

Courses React app is a courses management web application built using React. This project aims to provide users with a comprehensive solution for organizing courses and boosting productivity.

## Features

### Frontend

-   **Courses Management**: Create, edit, and delete Courses with ease. Organize them by authors, languages, due dates, and more.
-   **Authors Management**: Create, edit, and delete Authors of the Course.

## Installation

1. Clone the repository: `git clone https://gitlab.com/oleksandryanov/courses-app.git`
2. Navigate to the project directory: `cd courses-app`
3. Install dependencies: `npm install`
4. Start: `npm run start`

## Contributing

Contributions are welcome! If you have any suggestions, bug fixes, or new features to add, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or feedback, please contact me via email: oleksandr.yanov.eu@gmail.com.

