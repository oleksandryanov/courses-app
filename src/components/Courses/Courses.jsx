import './courses.scss';

import { useCallback, useState } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

function Courses({ courses, setCourseCreating, authors }) {
	const [searchValue, setSearchValue] = useState('');

	const search = (value) => setSearchValue(value.toLowerCase());

	const filteredCourses = useCallback(
		(courses) => {
			if (!searchValue) return courses;

			return courses.filter(({ id, title, description }) => {
				const cond1 = title.toLowerCase().includes(searchValue);
				const cond2 = description.toLowerCase().includes(searchValue);
				const cond3 = id === searchValue;

				return cond1 || cond2 || cond3;
			});
		},
		[searchValue]
	);

	return (
		<div className='courses'>
			<SearchBar search={search} setCourseCreating={setCourseCreating} />
			<div className='courses__cards'>
				{filteredCourses(courses)?.map(({ id, ...info }) => (
					<CourseCard
						key={id}
						{...info}
						authors={authors.filter(({ id }) => info.authors.includes(id))}
					/>
				))}
			</div>
		</div>
	);
}

export default Courses;
