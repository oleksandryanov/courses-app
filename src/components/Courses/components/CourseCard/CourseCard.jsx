import './courseCard.scss';

import { courseCardButtonText } from 'src/constants';
import { pipeDuration, pipeAuthors, pipeDescription } from 'src/helpers';

import Button from 'src/common/Button/Button';

function CourseCard({ title, description, duration, creationDate, authors }) {
	return (
		<div className='course-card'>
			<h2 className='course-card__title'>{title}</h2>
			<ul className='course-card__info'>
				<li>
					<b>Description:</b>
					<br />
					<span className='course-card__info-description'>
						{pipeDescription(description)}
					</span>
				</li>
				<li>
					<b>Authors: </b>
					<span className='course-card__info-authors'>
						{pipeAuthors(authors)}
					</span>
				</li>
				<li>
					<b>Duration: </b>
					<span className='course-card__info-duration'>
						{pipeDuration(duration)}
					</span>
				</li>
				<li>
					<b>Created: </b>
					<span className='course-card__info-date'>{creationDate}</span>
				</li>
			</ul>
			<Button buttonText={courseCardButtonText} />
		</div>
	);
}

export default CourseCard;
