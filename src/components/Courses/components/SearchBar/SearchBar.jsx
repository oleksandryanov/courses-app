import './searchBar.scss';

import { useState } from 'react';

import {
	searchPlaceholder,
	searchButtonText,
	createCourseButtonText,
} from 'src/constants';

import Input from 'src/common/Input/Input';
import Button from 'src/common/Button/Button';

function SearchBar({ search, setCourseCreating }) {
	const [value, setValue] = useState('');

	const onChange = (text) => {
		setValue(text);

		if (!text) {
			search(text);
		}
	};

	const onSearch = () => search(value);

	return (
		<div className='search-bar'>
			<div className='search-bar__search'>
				<Input
					value={value}
					onChange={onChange}
					placeholder={searchPlaceholder}
				/>
				<Button buttonText={searchButtonText} onClick={onSearch} />
			</div>
			<Button buttonText={createCourseButtonText} onClick={setCourseCreating} />
		</div>
	);
}

export default SearchBar;
