import './authors.scss';

import Button from 'src/common/Button/Button';

function Authors({ authors, courseAuthorsIds, addAuthor, deleteAuthor }) {
	const renderAuthors = (isCourse) => {
		return authors
			.filter(({ id }) =>
				isCourse
					? courseAuthorsIds.includes(id)
					: !courseAuthorsIds.includes(id)
			)
			.map(({ name, id }) => (
				<div className='author' key={id}>
					<span>{name}</span>
					<Button
						buttonText={`${isCourse ? 'Delete' : 'Add'} author`}
						onClick={() => (isCourse ? deleteAuthor(id) : addAuthor(id))}
					/>
				</div>
			));
	};

	const authorsAdded = renderAuthors(false);
	const authorsCourse = renderAuthors(true);

	return (
		<div className='authors'>
			<div className='authors_added'>
				{authorsAdded.length > 0 ? authorsAdded : 'None'}
			</div>
			<div className='authors_course'>
				{authorsCourse.length > 0 ? authorsCourse : 'None'}
			</div>
		</div>
	);
}

export default Authors;
