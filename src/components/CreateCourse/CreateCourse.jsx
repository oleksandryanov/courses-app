import './createCourse.scss';

import { useState } from 'react';

import Button from 'src/common/Button/Button';
import Input from 'src/common/Input/Input';
import Authors from './components/Authors/Authors';

import {
	createCourseTitleInputText,
	createCourseButtonSubmitText,
	createCourseDescInputText,
	createCourseDurationInputText,
	createCourseButtonAuthorText,
	createCourseAuthorInputText,
} from '../../constants';

import { pipeDuration, generateId, dateGenerator } from '../../helpers';

function CreateCourse({ addCourse, addAuthor, closeCourseCreating, authors }) {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState('');
	const [author, setAuthor] = useState('');
	const [courseAuthors, setCourseAuthors] = useState([]);

	const clearCourseInputs = () => {
		setTitle('');
		setDescription('');
		setDuration('');
		setAuthor('');
	};

	const createCourse = () => {
		if (!title || !description || duration < 1 || courseAuthors.length === 0) {
			alert('Fill all the fields!');
			return;
		}

		addCourse({
			id: generateId(),
			creationDate: dateGenerator(),
			authors: courseAuthors,
			title,
			description,
			duration,
		});

		clearCourseInputs();
		closeCourseCreating();
	};

	const createAuthor = () => {
		addAuthor({ id: generateId(), name: author });

		setAuthor('');
	};

	const addAuthorToCourse = (id) => {
		setCourseAuthors([...courseAuthors, id]);
	};

	const deleteAuthorFromCourse = (id) => {
		setCourseAuthors([...courseAuthors.filter((_id) => _id !== id)]);
	};

	return (
		<div className='create-course'>
			<button className='close' onClick={closeCourseCreating}>
				&#10060;
			</button>
			<div className='create-course__inner'>
				<div className='create-course__title'>
					<Button
						buttonText={createCourseButtonSubmitText}
						onClick={createCourse}
					/>
					<Input
						value={title}
						onChange={setTitle}
						placeholder={createCourseTitleInputText}
					/>
				</div>
				<div className='create-course__desc'>
					<Input
						InputTag='textarea'
						value={description}
						onChange={setDescription}
						placeholder={createCourseDescInputText}
					/>
				</div>
				<div className='create-course__duration'>
					<Input
						value={duration}
						onChange={setDuration}
						placeholder={createCourseDurationInputText}
						inputType='number'
					/>
					<p>
						Duration: <b>{pipeDuration(duration)}</b>
					</p>
				</div>
				<div className='add-author'>
					<Input
						value={author}
						onChange={setAuthor}
						placeholder={createCourseAuthorInputText}
					/>
					<Button
						onClick={createAuthor}
						buttonText={createCourseButtonAuthorText}
					/>
				</div>
				<Authors
					authors={authors}
					courseAuthorsIds={courseAuthors}
					addAuthor={addAuthorToCourse}
					deleteAuthor={deleteAuthorFromCourse}
				/>
			</div>
		</div>
	);
}

export default CreateCourse;
