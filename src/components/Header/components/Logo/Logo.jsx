import './logo.scss';

import logo from 'src/assets/logo.png';

function Logo() {
	return (
		<div className='logo'>
			<img src={logo} alt='logo' />
		</div>
	);
}

export default Logo;
