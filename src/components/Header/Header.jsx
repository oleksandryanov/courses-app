import './header.scss';

import Logo from './components/Logo/Logo';
import Button from 'src/common/Button/Button';

import { headerButtonText } from 'src/constants';

function Header() {
	return (
		<header>
			<Logo />
			<span>Bobby</span>
			<Button
				buttonText={headerButtonText}
				bgColor='#E29578'
				textColor='#EDF6F9'
			/>
		</header>
	);
}

export default Header;
