export const pipeDuration = (duration) => {
	if (duration < 0) return '00:00';

	const hours = Math.floor(duration / 60);
	const minutes = duration - hours * 60;

	return `${(hours < 10 ? '0' : '') + hours}:${('0' + minutes).slice(-2)}`;
};
