export const pipeAuthors = (authors) => {
	authors = authors.map((_) => _.name);

	return authors.length < 4
		? authors.join(', ')
		: authors.slice(0, 3).join(', ') + ' ...';
};
