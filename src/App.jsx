import { useState } from 'react';

import './app.scss';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedAuthorsList, mockedCoursesList } from './constants';

function App() {
	const [isCourseCreating, setCourseCreating] = useState(false);
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const addCourse = (course) => setCourses([...courses, course]);

	const addAuthor = (author) => setAuthors([...authors, author]);

	return (
		<div className='container'>
			<Header />
			<main>
				{isCourseCreating ? (
					<CreateCourse
						addCourse={addCourse}
						addAuthor={addAuthor}
						authors={authors}
						closeCourseCreating={() => setCourseCreating(false)}
					/>
				) : (
					<Courses
						setCourseCreating={() => setCourseCreating(true)}
						courses={courses}
						authors={authors}
					/>
				)}
			</main>
		</div>
	);
}

export default App;
