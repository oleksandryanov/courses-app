import './button.scss';

function Button({ buttonText, onClick }) {
	return (
		<button className='btn' onClick={onClick}>
			{buttonText}
		</button>
	);
}

export default Button;
