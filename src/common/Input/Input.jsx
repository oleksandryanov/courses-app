import './input.scss';

function Input({
	value,
	onChange,
	placeholder,
	labelText,
	inputType = 'text',
	InputTag = 'input',
}) {
	return (
		<>
			{labelText ? (
				<label className='label' for='input'>
					{labelText}
				</label>
			) : null}
			<InputTag
				type={inputType}
				placeholder={placeholder}
				className='input'
				value={value}
				onChange={(e) => onChange(e.target.value)}
				min='1'
				id='input'
			/>
		</>
	);
}

export default Input;
